Essa aula vai ser tão fácil que você vai conseguir completá-la de olhos fechados 😎 💤. Ok. Não tão fácil assim. Um operador ternário nada mais é que um `if - else` mais rebuscado que as vezes torna seu programa mais legível, e nós gostamos disso, não? A sintaxe do operador é dessa maneira: `let resultado = condição ? valorRetornadoSeTrue : valorRetornadoSeFalse`. Você vai utilizá-lo principalmente para atribuir valor a suas variáveis ou constantes. Vamos dar um exemplo:

```
let idade = prompt("Qual a sua idade? E não minta. Temos meios de averiguar")
let acessoPermitido = (idade >= 18) ? "Pode entrar" : "Acesso Barrado!!!"
alert(acessoPermitido)
```

Barbadinha né? Você pode encadear vários `? :` depois do primeiro condicional, exatamente como se fossem `if else` 🧠 . Um exemplo? Pode deixar:

```
let anos = prompt("Sua idade sem palhaçadas? Os métodos de averiguação falharam", 18)
let mensagem = (anos < 3) ? "Cuti cuti!" :
  (age < 18) ? "Olá mini adulto!"" :
  (age < 100) ? "Oi adulto!" :
  "Caramba! É você Gandalf ?"

alert(mensagem)
```
