# Variáveis     class_id 2

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Declaração de variáveis', 'SYS', 1, 'Como declarar variáveis com let e var', 'Para declarar uma variável você deve usar ou palavra-chave **var** ou **let** e após, o nome da sua variável. Você pode ter ouvido que o uso da palavra **var** caiu em desuso e não é mais usada, mas ela continua tendo seu uso em situações especiais. Javascript é uma linguagem muito flexível 🤸 e **var** ainda tem seus usos. Você verá a diferença entre as duas quando abordarmos escopo ⏳. Vamos usar a palavra-chave **let** , que é mais amigável para iniciantes e declararmos nossa primeira variável. Digite no console do browser:\
`let linguagem`\
O que isso representou 🤷? O que essa instrução realizou no seu programa🙇‍? Ela dedicou um "pedacinho" 🍕 de memória RAM para a sua variável. Pense como uma gaveta em um arquivo em que você tenha colocado um post-it com um nome (no caso `linguagem`) e reservado só para o seu uso 🗄📁. O arquivo é a memória RAM total do computador e você reservou uma gaveta (variável) com um nome que você escolheu e que vai armazenar o que você achar útil para o seu programa.

Mas vamos além. Uma variável só tem utilidade se tem um valor em seu conteúdo, e vamos fazer exatamente isso. Digite `linguagem = "Javascript"`. Você acabou de atribuir o valor **Javascript** para sua variável. Pense que dentro da gaveta agora há algo valioso. Mas você pode estar pensando: "E se eu quiser alterar o valor da minha variável?" Isso é perfeitamente possível. Digite `linguagem = "Python"`, e você verá que agora o valor de linguagem mudou para Python.
E se você quiser colocar um número dentro de `linguagem`?
Sem problemas, embora as boas práticas de programação aconselhem o contrário isso é só um teste 😉 . Digite `linguagem = 42`, e veja como sua variável mudou de um texto para um número. Se você duvida digite `typeof linguagem` agora, digite `linguagem = "Javascript"` e `typeof linguagem`. Isso ocorre porque Javascript é uma linguagem fracamente tipada, diferente de C/C++, Java, C# e outras 🥱.

Você declara suas variáveis e o interpretador de Javascript vai mudandando o tipo dependendo do uso que você faz de suas variáveis. Aliás há uma linguagem derivada de Javascript que é fortemente tipada. Isso que você fez com a variável linguagem de mudar para um número e depois de volta para um texto seria proibido 🚫. Essa linguagem tem o nome de **TypeScript** e seu uso tem crescido recentemente.

Mas vamos quebrar o gelo 🧊🔨 e fazer alguns exercícios? Não? Você não quer quebrar o gelo ? É simples, pegue um picador de gelo e deposite o gelo em uma superfície estável, preferencialmente um balde de metal (Estou pensando e isso pode ser um tanto perigoso. Não faça isso em casa 🤕 ) Você estava preocupado com os exercícios não é 🙄 ? Relaxe e pense no que você viu nesse tutorial OK?
E mais uma outra coisa, podemos pedir para você 🥺? De agora em diante vamos chamar Javascript de **JS** para economizar texto em tela. Isso é economia no banco de dados! 8 bytes por vez que mencionamos Javascript é uma tremenda economia 🤨...Mas, o texto deste parágrafo inteiro já consumiu muitas vezes mais do que centenas de menções a Javascritpt, você pode dizer. E isso é verdade 🙄. Nós somos preguiçosos, ou melhor, eficientes e digitar **JS** é mais eficiente do que digitar Javascript. Não se esqueça, de agora em diante Javascript também será chamado de **JS**.
'
, 2);
INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Entrada e Saída', 'SYS', 2, 'Como realizar entrada e saída de dados, Comentários', 'Legal, você já sabe como declarar uma **variável** em JS, como atribuir um **valor** a ela e como ver o tipo dela. Você se lembra da palavra-chave `typeof` 🧠 ?. Agora eu sei que você já quer utilizar suas variáveis em jogos super divertidos, como por exemplo o número de vidas que seu personagem tem: Poderíamos declarar no console do browser: `let vidas`. Se ele começa com 5 vidas podemos iniciar com o valor 5, então `vidas = 5` (note que 5 está sem aspas em sua volta. Isso vai ser importante no futuro ⏳ ). Se o seu personagem morreu 2 vezes, e o seu jogo alterou o valor de `vidas` e seu personagem não pegou nenhum 🍄 verde, você pode inspecionar o valor de uma variável no console do browser com a seguinte instrução `alert(vidas)`. O valor a ser impresso no console seria 3 no caso, a menos que estejamos em uma matemática estranha e 5 - 3 não seja mais igual a 2 😜 .

Ou saindo da ideia de jogo. Faça:\
`let altura = 1.65 `\
`alert("Eu tenho " + altura + " de altura")`

Atente-se que você deve utilizar ponto para separar a parte inteira da fracionária, e isso ocorre na maioria das linguagens de programação, porque adotam o sistema inglês de numeração 💂 . Sinta-se à vontade de colocar a altura que você possuir no valor da variável. A última coisa que você deve ter notado são as aspas e o texto adicionado dentro do parênteses. Estamos intercalando texto com o valor da sua variável com mais algum texto com o operador `+` (também em breve ⏳ ). O ponto é que você deve usar aspas simples ou duplas quando inserir texto entre os **parênteses** de uma instrução `alert`. Poderiámos ter escrito com aspas simples.\
`alert(\'Eu tenho \' + altura + \' de altura\')` Com o mesmo efeito.

Há uma maneira, digamos, menos espalhafatosa, de realizar a sáida de suas variáveis. Ela é feita com a instrução `console.log()`, de forma similar ao alert. A diferença é que uma janela não pula na sua cara, e você pode usar `,` ao invés de `+` quando tiver mais de um dado de saída.\
`console.log("Eu tenho ", altura ," de altura")`Repare nas vírgulas

Até aí tudo bem, você consegue vazer os dados saírem do computador 💻 ▶🎲 . Agora como realizar a entrada de dados em JS? Afinal um programa não serve para muita coisa sem entrada de dados 💻 ◀🎲 pelo usuário. É para isso que serve a instrução `prompt()`. Você pode usá-la colocando uma pergunta dentro dos parênteses, com aspas simples ou duplas, e armazenar o que o usuário digitou logo em seguida em uma variável. Por exemplo:
`let gameFavorito = prompt("Qual o seu jogo favorito ever?")`\
`alert("seu jogo favorito é " + gameFavorito)`
Você pode colocar um valor padrão no prompt logo após a frase da pergunta. Assim:\
`let curso = prompt("Qual o seu curso?", "ADS")`\
`alert("O seu curso é " + curso + " mesmo? Legal.")`

Outra maneira de interação com o usuário é o `confirm()`. Você coloca uma pergunta entre os parênteses e a janela modal irá perguntar "OK" ou "Cancel". O resultado poderá alterar o fluxo do seu programa, quando vermos **condicionais** ⏳ . Assim:\
`confirm("O sentido da vida é 42?")`

Mais uma coisa, por favor não durma ainda ⏰😴? Precisamos falar de comentários. Não o que as pessoas comentam na rua diariamente, isso são fofocas. Comentários na programação servem para que você explique o raciocínio do seu algoritmo ou programa, e ajudam na manutenção do mesmo. Eles não interferem em nada na execução do programa. Você pode comentar em JS de duas maneiras:

- Com `//` (duas barras) para comentários de uma linha;
- Com `/* */` (barra, asterisco e depois asterisco, barra para fechar o comentário) para comentários de múltiplas linhas.

Na prática você pode fazer assim

`//essa variável irá armazenar o XP do personagem`\
`let xpPersonagemPrincipal = 100`

ou

`/* Esse jogo vai ser um RPG de turnos`\
`O jogador poderá ter 5 personagens ao mesmo tempo`\
`O personagem principal inicia com 100 de XP`\
`Toda a lógica do jogo virá a seguir */`

No começo pode ser um pouco trabalhoso comentar 🏋 , mas com a prática você verá que seus programas ficarão muito mais legíveis e de fácil manutenção 🔧🚗.', 2);
INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Nomes', 'SYS', 3, 'Como nomear e O QUE NÃO FAZER ao nomear suas variáveis', 'Há mais algumas coisas que você precisa saber antes de decolar 🛫 como um programador JS. Você já sabe declarar variáveis e ver seus valores no console com a instrução `alert(variavel)` 🧠 . Note que o nome da variável declarada não é `variável`. Tente fazer isso no console do browser:\
`let variável = 10`\
E estranhamente isso funciona. 😱
Até algumas edições do JS atrás acentos gráficos no nome de variáveis, tais como `^~ç´` eram **proibidos**. Mas saiba que essa modificação é bem recente então encorajamos você a **SÓ USAR LETRAS E NÚMEROS NOS SEUS NOMES SEM ACENTOS GRÁFICOS**, com a exceção dos caracteres `$` (_dollar_ ou "cifrão") e `_` (_underline_ ou "sublinhado"). Mais algumas **regras** se aplicam . Calma, eu sei que regras são chatas, mas se queixe ao inventor do Javascript que as definiu 😏 . Elas são:

1. Não **começarás** o nome da tua variável com um número. Então o nome `5livros` é **ILEGAL** e não batizarás variável assim;
2. Poderás utilizar de **letras**, **números** (não no começo, se lembre da regra inicial ☝️), os caracteres **$** e **\_** para batizar vossas variáveis;
3. Não usarás espaços no nome das tuas variáveis. **NÃO FAÇA** `let nome do personagem`
4. Não usarás palavras reservadas da linguagem, tais como **let**, **var**, **function**, **new**, **return**.

Isso ficou um tanto dogmático e bíblico, mas era só para fixar melhor na sua cabeça e esperamos não ter ofendido nenhum religião 🙏 . Então **É PERMITIDO**:\
`let _limite`\
`let VidaMinimaPersonagem`\
`let caixa10dolares`\
`let dolar$euroCotacao`\
`let __sublinhados` (dois ou mais sublinhados são permitidos no início)\
`let $$dinheiros` (mesma coisa com o caractere de cifrão)

**NÃO FAÇAS**:\
`let maçãs` (embora as novas versões do JS permitam acentos gráficos, isso não é recomendado)\
`let 100reais` (não se pode começar um nome com um número)\
`let @email` (outros caracteres diferentes de `$` e `_` não funcionam)

E quanto a **maiúscúlas e minúsculas**? Faz diferença escrever `MeuPersonagem` e `meupersonagem`? A resposta é um sonoro 📣 **SIM**. Em JS e na grande maioria das linguagens faz muita diferença. Você está declarando duas variáveis diferentes se usar o **mesmo** nome só mudando entre **maiúsculas e minúsculas**. Chamamos sistemas que dão essa diferença de tratamento às palavras com essa diferenciação de **CASE SENSITIVE**. Então fique atento nos seus programas de que maneira você declarou suas variáveis.

Uma maneira boa de nomear suas variáveis se chama **camelcase** 🐫 e o nome vem das corcovas de um camelo mesmo. você começa com uma palavra com letras minúsculas e depois a próxima palavra terá só a primeira letra maíuscula e o resto minúsculo, e assim por diante. Assim:\
`let minhaPrimeiraVariavel`\
`let vidasJogadorUm`\
`let tituloFilmesAlugados`\
`let mensagemFinalJogo`\
Todos são bons nomes de variáveis que utilizam o sistema **camelcase**.'
, 2);
INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Tipos I', 'SYS', 4, 'Tipos numérico, texto e lógico', 'Você está no caminho certo para se tornar um programador JS bem sucedido. Mas antes de se aventurar por condicionais e operadores você precisa dominar o segredo dos **tipos** em JS 🕵 . Na verdade não há nenhum segredo e eles estão bem documentados na página de publicação das especificações do **ECMAScript** ( nome oficial por o qual nosso querido JavaScript é chamado). Mas a documentação oficial é bem maçante, e acredito que tenha chamado a sua atenção com a história de segredo 😒 .

Só um momento! Você deve estar pensando 🤔 . JS não é uma linguagem **fracamente tipada**? Porque eu devo saber sobre tipos? Isso vai fazer diferença na minha vida como programador JS? E a resposta infelizmente, ou felizmente dependendo do prisma 🔺🌈 que se observa é **SIM**, você precisa saber sobre tipos em JS. Por mais que ela seja uma linguagem fracamente tipada, **internamente** existem tipos e como você viu anteriormente lá no primeiro tutorial 🧠 , a palavra-chave `typeof` permite averiguar o tipo de uma variável. A verdade é que JS é da família de linguagens chamadas **de tipo dinâmico** ou **dinamicamente tipadas**, assim como Python. Mas vamos seguir e ver quais são esses tipos:

# Number

Já vimos esse tipo antes, e ele é um número 😒 . Até aí nenhuma surpresa. Ele pode ser um número **inteiro** ou **fracionário**. Você pode declará-los assim:\
`let num = 123`\
`let pi = 3.1416`

Agora temos um caso interessante se você tentar dividir um número positivo por zero. Tente no console do browser:\
`alert(1/0)`\
E espero que o resultado tenha sido `Infinity`, que é um número infinito positivo (aqueles que estudaram Cálculo entenderão quando se diz que um número tende ao infinto positivo 🤓 ). Agora faça a mesma coisa, mas com -1 ao invés de 1. `alert(-1/0)`. Temos `-Infinity` que seria um número infinito negativo 🤯 .

E por fim temos o `NaN` de **_Not a Number_** ou **_Não é um Número_**, que curiosamente é do tipo Número (um pouco paradoxal se você parar para pensar 🤔 . Talvez os criadores do JS não quiseram criar mais um tipo só para esse caso específico). Ele acontece em situações especiais, quando você tenta utilizar um número com um outro tipo de variável com um operador (mais sobre operadores em breve ⏳ ) que não faz sentido para esses dois tipos.
Como por exemplo:\
`alert("Isso é um texto. E textos não são divisíveis por números"/2)`\
Isso ocorre porque o operador `/` não sabe o que fazer quando encontra um texto e um número, e a melhor resposta que ele pode dar a você é que "Não, isto não é um número !", isto é um `NaN`.

# String

Também já vimos esse tipo antes 🧠 . Quando colocamos qualquer coisa entre aspas duplas ou simples vocês está criando uma String em JS. Mesmo que você não a salve em uma variável, o interpretado de JS cria por baixo dos panos, sorrateiramente 😼 uma **variável de ambiente** com qualquer valor que você tenha colocado entre aspas (duplas ou simples). Então `alert("Esse é um texto qualquer")` está criando uma **variável de ambiente** com o conteúdo `Esse é um texto qualquer` do tipo **string**, mesmo que você não tenha declarado uma variável para isso. Você vai ver um conceito que se aproxima de variáveis de ambiente na medida em que são imutáveis logo em breve ⏳ (e dessa vez eu vou dar uma dica , elas são são **const**antes).

Agora nas versões mais recentes do JS temos uma nova e mais versátil versão de strings. Elas são principalmente utilizadas em **outputs** de variáveis, como o ... Você se lembra ? 🧠 Sim, nosso querido `alert()`. Você pode utilizá-las assim:\
`let nome = "Joãozinho"`\
`` alert(`O ${nome} não vai na aula hoje`) ``\
Ou seja, a variável que estiver depois do cifrão e entre as chaves `${}` será mostrada, e se você acha que pode usar um número no meio das chaves, você está redondamente CERTO. Tente:\
`let centena = 100` (observe que não temos aspas para declarar números)\
`` alert(`Uma centena tem o valor numérico de ${centena}`) ``

# Boolean

Esse tipo só tem dois valores: verdadeiro ou falso. Ou melhor `true` ou `false` em JS. Uma analogia 🙄 , eu sei que você está farto de analogias, é de um interruptor de luz. Ele só tem dois estados: ou está **desligado** ou **ligado**. Uma frase do tipo "A mulher está grávida 🤰 " ou é verdadeira ou falsa, não há um meio termo. Vamos nos adiantar um pouco no conteúdo e mostrar um operador, mas não conte para ninguém 🤫 . Se você colocar:\
`alert(0 > 1)`\
Você vai achar o valor `false`, que é do tipo booleano no seu console. Agora inverta o operador 🤷..Em vez de maior coloque menor como:\
`alert(0 < 1)`\
E o resultado vai ser `true`, porque 0 evidentemente é menor que 1. Agora para o que eu vou usar uma variável booleana nos meus programas, você deve estar se perguntando?
Bom, há vários motivos. e nós vamos listar alguns:

- Para utilizar como **flags** em seus programas. Flags são variáveis que sinalizam um comportamento, que pode ser falso/verdadeiro. Mas qualquer lógica binária pode ser adaptada para utilizar uma variável booleana. Pensando em um jogo, poderiámos usar uma variável booleana para simbolizar o estado vivo/morto de um personagem. `true` seria equivalente a vivo 😇 e `false` equivalente a morto 👻 . Dependendo do valor dessa variável o seu jogo poderia adotar um comportamento diferente.
- Para aplicar lógicas mais complexas em seus programas (como utilizar **OU**, **E**, **NOT** . Em breve ⏳ ) , e entender o resultado de todas essas operações lógicas.
- Para todas as partes no seu programa que não sejam números e textos e envolvam lógica de programação garantimos que uma variável booleana dará conta, desde que seu algoritmo esteja bem estruturado.'
, 2);
INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Tipos II', 'SYS', 5, 'Tipos objeto, indefinido, e nulo', 'Estes foram os tipos mais comuns que você vai utilizar na sua vida inicial de programador JS, mas existem mais tipos (calma, não nos xingue 🤗, você não queria ser um programador JS?). Tome um 🧉 ou um ☕ e vamos seguir desbravando os tipos, em ordem de importância.

# Objects

Sem colocar os bois na frente da carroça, podemos dizer que um objeto em JS é um tipo de dado mais complexo que um **Number**, uma **String** ou um **Boolean** 🧠. Os tipos mencionados antes são conhecidos como **primitivos** e um Objeto é um tipo que faz uma referência. Mas na prática é que você pode designar propriedades para o seus objetos, de maneira que seus dados fiquem mais **organizados**. Para declarar um objeto podemos declarar um objeto vazio, ou um objeto e já preenchê-lo com atributos.
Declarando ele vazio:\
`let carro = {}` (Observe as chaves)\
E o preenchendo com propriedades\
`carro.cor = "amarelo"`\
`carro.velocidade = 250`\
`` alert(`Meu Camaro ${carro.cor} atinge a velocidade total de ${carro.velocidade}.`)``\
Ou declarar um já com as propriedades:

<code>
let carro2 = {
    cor : "vermelha",
    velocidade : 335
}
alert(`Minha Ferrari ${carro2.cor} atinge a velocidade total de ${carro2.velocidade}.`)
<code>

Em breve você verá mais sobre Objetos (o quê? Você está esperando o emoji da ampulheta que parece uma maçã? Ou você está de saco cheio com "Em breve isso", "Em breve aquilo"?). Em breve você saberá o porquê da paciência na programação ⏳😜 . Não , a verdade é que o seu, o meu, o cérebro de todo o ser humano tem um limite de aquisição e de relacionamento de informação. Então achamos melhor dar a você uma ideia do que será o material das próximas aulas do que despejar informação que talvez você não consiga assimilar no momento. OK?

# Undefined

Esse tipo é o pesadelo da maioria dos programadores JS . Ele quer dizer que a sua variável não possui um valor atrubuído. Com programas mais simples ele pode acontecer se você declarar a sua variável utilizando a palavra-chave `let` e se referindo a uma variável que não possui ainda um valor atribuído. Digamos que você cometa um erro de digitação no uso de sua variável. Por exemplo:\
`let nome = "meuNome"`\
`alert(NOME)`(Veja que você está se referindo a uma variável inexistente)\
Você verá que ocorrerá um erro no seu console `NOME is not deffined`. E se você fizer:\
`alert(typeof NOME)`\
ou\
`alert(typeof qualquerVariavelSemValor)`\
Você terá como retorno no seu console `undefined`. Isso ocorre também se você só declarar uma variável e não atribuir um valor a ela. Como:\
`let idade`(Repare que você declarou e não atribui nenhum valor)\
`alert(idade)`\
Essas são as maneiras mais comuns de se encontrar com o valor `undefined`, mas JS tem um estranho jeito de frustar suas expectativas como um programador iniciante. Se você achar essa mensagem `Uncaught ReferenceError: NomeDaSuaVariavel is not defined` ou achar o tipo `undefined`, mantenha a calma 🤞 e revise as atribuições de suas variáveis.

# Null

Esse tipo tem seus usos, relacionando-se principalmente como o tipo **Object**. O tipo `null` quer dizer que um objeto ou outra variável de referência perdeu sua referência. Ela é um jeito melhor de dizer que uma variável é **vazia**, **nada** ou **valor desconhecido** do que `undefined`. Ela serve para tipos de referência. Temos tipos primitivos e tipos de referência em JS.

Nesse momento é interessante você saber que os tipos **primitivos** são os tipos mais básicos em JS e são:

- Numbers
- Strings
- Booleans
- BigInts (um tipo introduzido recentemente para permitir números **MUITO GRANDES** . Pense no poder do Goku no final de Super Dragon Ball)
- Null
- Undefined
- Symbol (um tipo específico utilizado para identificar Objetos)

E os tipos que não são primitivos 🗿 ou tipos "complexos" 🧬 (não chame-os de complexados, eles são sensíveis quanto a isso 🤫 ):

- Objects
- Functions (funções que são como mini-programas reutilizáveis ⏳ )'
, 2);
INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Constantes', 'SYS', 6, 'O que são e para o que servem Constantes', 'Você já viu como declarar uma variável com a palavra **let**, e sabe que pode modificar o seu valor a qualquer momento 🧠 . Por exemplo:\
`let melhorJogoCriado = "Zelda"`\
`melhorJogoCriado = "GTA V"`
Você viu até que é possível, embora não aconselhável mudar o tipo de uma variável. Como:\
`let numero = 1`\
`numero = "Um texto qualquer"` (repare nas aspas)

Mas digamos que em seu programa ou jogo, certas medidas ou quantidades serão **constantes**, por exemplo o seu programa desenha um círculo e para isso você vai precisar do valor de pi. Você está com preguiça de pegar uma calculadora (e vamos fingir que nem no seu celular, nem no seu computador existe uma calculadora 😒 ) e se lembra que pi é em torno de 3. Então você declara\
`const PI = 3`\
E utiliza essa constante por todo o programa quando precisar. Você irá notar, neste exemplo hipotético, que os seus círculos ficaram estranhos. Isto é porque utilizamos um valor aproximado de pi. "E agora !!! Meu programa não presta para nada 🤬 ": o seu eu hipotético estará gritando. Calma, há uma solução fácil para isso. Vá lá na declaração de pi, no início do programa e mude a **atribuição de valor** de pi. Mude a linha\
`const PI = 3`\
para\
`const PI = 3.1416`\
E agora todos os lugares no seu programa que usam essa **constante** estarão com esse novo valor, e seu círculo estará bem redondinho. Agora você pode estar se perguntando "Eu poderia fazer a mesma coisa com let", e isto é verdade; mas há uma vantagem que o uso de const dá a você como programador: você não pode mudá-las após a sua declaração. Neste exemplo o que mudamos foi o **valor na declaração** e não o **valor após a declaração**, e isso faz muita diferença. Tente modificar o valor de `PI` para 5.\
`PI = 5`\
E espero que o erro `Assignment to constant variable.` tenha aparecido no seu terminal. O uso de constantes impede que você ou outro programador que esteja trabalhando no mesmo programa mude o valor de uma variável que deveria ser imutável. Geralmente são textos, booleans, quantidades, parâmetros que permitem que o programa funcione; mas que não se deseja que sejam alterados, a não ser em situações específicas como foi o caso do programa do círculo.
Resumindo:

- Use **const** quando você for utilizar um mesmo valor em vários lugares em seu programa e ele não for alterado pela lógica de seu programa.
- Use **const** para a proteção do valor em suas variáveis (eu sei que essa frase é paradoxal, mas não há maneira melhor de se referir a endereços nomeados na memória do que variáveis).

Á 😮 ! e mais um detalhe que você deve ter percebido: as constantes são nomeadas com **todas as letras em maiúsculas**. Você pode nomeá-las de outra forma, mas é uma ótima prática de programação nomear de acordo com essa regra. E depois ficará mais fácil achar as suas constantes quando os seus programas e jogos ficarem gigantes 👩‍💻 ▶🔟🔟🔟🔟 .',
2);

# Classe Operadores e Conversoes     class_id 3

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Operadores Matemáticos', 'SYS', 1, 'Principais Operadores Matemáticos do JS', 'Você já deve ter visto estes operadores na sua vida certamente 👶 , mas não no contexto da programação. Eles funcionam de maneira muito parecida com o que você viu nas suas aulas de matemática ou como eles funcionam em uma calculadora. Faça:
`let a = 10`\
`let b = 5`\
`alert(a + b)`

E espero que 15 tenha aparecido no janela do seu browser. Até aí tudo normal. Temos também `- * /`\
Faça:
`alert(a - b)`\
`alert(a * b)`\
`alert(a / b)`\
E as respostas são 5, 50 e 2. "Essas foram fáceis, quero ver fazer 134.534 \* 45.667 / 73.856 !!!" 🤬 . Não se esqueça que um computador é uma máquina muito boa com números. Essa operação é feita em menos de um milésimo de segundo 😲 . Cabe a você como programador\ra traduzir fórmulas e conceitos matemáticos para uma linguagem de programação. E a propósito, a resposta da última pergunta é 83.185,715 aproximadamente.

Além desses operadores que você já deve ter visto, temos o operador **unário** `-` que troca de sinal um número (um número positivo vira negativo, por exemplo). Faça:
`let x = 42`\
`x = -x`\
`alert(x)` E o valor de x mudou de 42 para -42.
Ainda temos dois operadores que você não deve ter visto na escolinha, o `%` e o `**`. O primeiro retorna o resto de uma divisão e o segundo eleva um número a uma potência 🤯 . Calma, vamos mostrar alguns exemplos. Faça:\
`alert(5 % 2)` 5 / 2 é 2 com resto 1\
`alert(42 % 5)` 42 / 5 é 8 com resto 2\
`alert(2 ** 2)` 2² é 4\
`alert(3 ** 3)` 3³ é 27\
Legal? Mas a coisa vai ficar mais divertida 🥱 . Eu juro que vai. Matemática é divertida (se esqueça da sua professora chata que pegava no seu pé quando você dormia na aula de logarítmos). Não? bom, eu tentei.
Digamos que você queira usar uma variável para uma operação matemática e conservar o resultado na mesma variável. algo assim:
`let n = 2`\
`n = n + 5`\
Há um atalho para fazer isso em apenas uma linha. Dessa forma:\
`n += 5`\
E das duas maneiras n será 7. Isso poupa um pouco de escrita de código, e quando você se acostumar ficará até mais legível. Todas as outras operações `- * / % **` podem usar dessa forma de atalho. Como desafio tente para os outros operadores no seu terminal, com o outro número sendo 5 (lembre-se de reatribuir o valor de n para 2 se você as for fazer em sequência). As respostas vão ser 2, -3, 0,4 , 2 e 32 respectivamente.

Temos um outro atalho matreiro 😼 que JS nos dá. Se você quiser incrementar uma variável em 1, você pode fazer:\
`let i = 1`\
`i = i + 1`\
Ou da maneira que você acabou de aprender\
`i += 1`\
Mas há uma outra maneira ainda mais elegante\
`i++`\
Todas vão incrementar 1 em uma unidade. Que número é 1 + 1 😒 ? Mas o principal é que sempre que você querer incrementar em 1 a sua variável você pode fazer `variavel++`. Isso é muito útil em **contadores**, que são variáveis que contam 😠 quantas vezes determinada situação aconteceu no seu programa. Há uma diferença entre `variavel++` e `++variavel` (++ antes do nome). Mas isso fica para depois ⏳ .
',
3);

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Comparações', 'SYS', 2, 'Comparadores Igual a, Maior/Menor que', 'Nós acabamos de ver operadores que fazem operações matemáticas. Mas nós não conseguimos programar softwares complexos ou jogos só com operadores matemáticos. Imagine que seu program ou jogo tenha que checar se alguma variável atingiu determinado valor, ou uma variável Boolean está verdadeira. Aí que entram os **operadores de comparação**. Temos basicamente esses operadores, que também devem lembrar o que você deve ter visto nas suas adoradas aulas de matemática 😑 :

- Igual a `a == b`, e repare nos dois caracteres `=`, para não se confundir com o operador de atribuicão. Isso é um erro **MUITO COMUM** para quem está iniciando, e até mesmo para quem se diz avançado 👽 . Quando os dois **operandos** (que são as variáveis dos dois lados do operador, no caso `a` e `b`) são iguais, esse operador retorna verdadeiro, ou em JS `true`;
- Maior/Menor `a > b, a < b`. O primeiro verifica se a é maior do que b, e o segundo se a é menor do que b. Bem simples não? No primeiro caso se `a` for menor do que `b`, o operador retorna `true`. No segundo caso... Você pegou a ideia né? Você é um/a aluno/a inteligente.
- Maior ou igual/Menor ou igual `a >= b, a <= b`. Semelhante ao anterior, mas e isso é **IMPORTANTE**, também é verdadeiro no caso dos operandos serem iguais. Exemplos:\
  `5 >= 7` false\
  `0 <= 1` true\
  `-1 <= 0` true\
  `42 <= 42` true, porque os dois são iguais\
  `42 < 42` false, obviamente\
  Acho que você pegou o jeito. A diferença entre esses dois operadores vai ficar mais clara quando entramos em Repetições e Laços ⏳.
- Diferente de `a != b`. Esse operador verifica se os dois operandos são diferentes, e se for o caso, retorna `true`. Ele faz exatamente o contrário do operador igual `==`.

E esses operadores podem ser usados em textos ou Strings em JS? A resposta é sim, mas os resultados são um tanto estranhos 😵 (exceto o comparador de igualdade `==` e o de diferente de `!=` ). Basicamente JS compara as duas Strings por ordem alfabética, letra por letra, mas há alguns detalhes no algoritmo que levam a conclusões não tão óbvias. De qualquer maneira você pode tentar no seu terminal algumas comparações com Strings, mas não vamos abordar esse assunto aqui.

Há um último detalhe, e isso se aplica especificamente a JS. Como a linguagem é fracamente tipada, alguns bugs aconteciam, ou por descuido do programador, ou por detalhes na implementação do JS que convertem o tipo por debaixo dos panos 😼 .
Se você fizer:\
`alert(0 == false)`\
`alert("" == false)`\
`alert(1 == true)`\
Você terá `true` em todos os casos 🤯 . Isto ocorre porque JS converte tipos com o uso de alguns operadores. Você verá mais sobre conversão de tipos em uma próxima aula. Para garantir que você quer saber que as variáveis tem os valores iguais e seus tipos também são iguais temos o operador `===` ou **Strict Equality** (fale com um sotaque britânico 💂 para ficar mais pomposo). Ele assegura que além do valor, o tipo tem que ser igual. então:\
`alert(0 === false)`\
false, porquê são de tipos diferentes (Number e Boolean)\
`alert("" === false)`\
false também, (String e Boolean)\
`alert(1 === true)`\
false também, (Number e Boolean)\
Para finalizar, temos o **Strict non-equality** `!==` que é análogo ao operador diferente de `!=`, mas assegurando que além dos valores, os tipos também são diferentes.',
3);

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Conversões entre Tipos', 'SYS', 3, 'Conversão entre tipos diferentes em JS', 'Você viu na aula anterior que nosso querido JS, apesar de ser uma ótima linguagem de programação é um pouco sacana 🤡 no que se refere a **tipos**. Ele tem uma tendência grande a converter tipos diferentes, e os resultados podem deixar você perplexo. Mas não se você estudar um pouco como o JS lida com os tipos quando usamos seus **operadores**.
Para aquecer 🧣🧤 vamos falar sobre um operador que nós já vimos, o `+`, mas em outro contexto, com **Strings**. Você pode concatenar (juntar) duas ou mais strings utilizando esse operador. Por exemplo:\
`let a = "Goku"`\
`let b = "Naruto"`\
`let c = "Luffy"`\
`alert(a + ", " + b + " e " + c + " são meus personagens de anime favoritos")`Se você não gostou do exemplo, coloque os seus personagens 😝 .\
Bem legal, não? Repare que cada `+` precisa ter duas Strings ao seu lado, e que a vírgula e o "e" foram colocados em aspas duplas (poderiam ter sido aspas simples). Agora vem a parte estranha. Se você fizer:\
`alert("1" + 2)`\
12 aparecerá no sua janela do browser. Isso ocorre porque JS converte o tipo Number para String quando fazemos uma concatenação entre os dois. O operador `+` deixa de ser um operador de soma e vira um operador de concatenação quando encontra um String.
Mas ele tem ordem de precedência, então:\
`alert(2 + 2 + "1")`\
é igual a "41" e não "221", porque a soma entre Numbers está localizada antes do operador de concatenação de Strings. Agora:\
`alert("1" + 2 + 2)`\
é igual a "122" e não "14", porque o JS transforma tudo em String após a primeira concatenação (lembre-se que o número 2 é convertido para String automaticamente).

Você pode forçar alguma variável ou valor a se transformar 🦸 em um tipo. Vamos ver como transformar para Number, String e Boolean.

# Number

Você pode forçar um valor a ser um número de várias maneiras. Por exemplo:\
`alert("6"/"2")` Converte as Strings para Numbers e faz a divisão e imprime 3\
`let a = false`\
`a = +a`\
`alert(a, typeof a)`\
Aqui utilizamos o operador unário `+` para converter um Boolean para um Number. Temos mais uma maneira, utilizando a função `Number()`. Assim:\
`let str = "123"`\
`let num = Number(str)`\
`` alert(`número de ${num} de tipo ${typeof num}`)``
E se a String conter algo que não for um número?\
`let titulo = Number("Just Dance 2021")`\
`alert(titulo)`Você se lembra do que significa NaN ?\
Outras conversões são possíveis:\
`alert(Number(true))` 1\
`alert(Number(false))` 0\
`alert(Number(null))` 0\
`alert(Number(undefined))` NaN\
`alert(Number(""))` String vazia é igual a 0\
`alert(Number(" 42 "))` 42

# String

Se com números as coisas são um pouco complicadas, com Strings são bem fáceis. Tudo é convertido para um texto, seja `true`, `false`, `null`, `undefined`, `42`, `3.1416`. Tente:\
`let valor = String(null)`\
`` alert(`conteúdo: ${valor}, do tipo: ${typeof valor}`) ``

# Boolean

Aqui se aplica uma regrinha:
Todos os valores que por intuição consideramos "vazios" se tornam **falsos** ou `false`. Isso inclui:

- 0 (O número zero)
- "" (uma String vazia)
- null
- undefined
- NaN

Todo o resto vira verdadeiro ou `true`. Então:\
`Boolean(1)` true\
`Boolean(0)` false\
`Boolean("")` false\
`Boolean("texto")` true\
`Boolean("0")` true\
`Boolean(" ")` true, por causa do espaço no meio\
`Boolean(null)`false\
`Boolean(undefined)`false\
`Boolean(NaN)`false

Ufa ! 😅 acabamos essa aula. Eu já vejo os seus poderes aumentarem. Siga treinando 🏋 e na próxima aula eu irei ensinar como aplicar a técnica do Kamehameha 🌠. Não? Serão conceitos de programação mesmo 😶 .',
3)

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Funções Matemáticas Avançadas', 'SYS', 4, 'Funções da Classe Math', 'Você aprendeu os operadores matemáticos comuns como `+ - * /`. E até alguns mais avançados, como `%` e `**` : você se lembra do que eles fazem?
Mas e se o seu programa (a gente sabe que será um jogo 😉 ) envolver conceitos de física avançada 🚁 . Como você irá simular tudo sem que um desastre aconteça 🎇🪂 ? Talvez seja bom você dar uma olhada nas **funções matemáticas** que JS nos dá para fazer uma física top (claro e depois fazer um mestrado e um doutorado em Física, olha acho que é melhor usar um engine pronta...Não! Você consegue! Vamos lá 👩‍🔬 ).

Nós conseguimos usar algumas funções mais avançadas utilizando a classe **Math**. Você chama essa classe e depois uma função dessa classe, por exemplo para encontrar a raiz quadrada de um número `Math.sqrt(9)` e temos 3 como resposta. Vamos ver as funções mais úteis:

`Math.abs(num)` Retorna o valor absoluto de um número, ou seja, se for negativo, ele será convertido para positivo. Se ele já for positivo se manterá igual. Faça:\
`Math.abs(-42)` 42, o sentido da vida. Captou a referência? OK. Vamos parar de usar referências ao "Guia do Mochileiro das Galáxias". Nem é um livro tão legal assim.

`Math.ceil(num)` Arredonda um número com vírgula para cima. Funciona melhor dando um exemplo. Faça:\
`Math.ceil(4.2)` 5. O que? Quebramos a nossa promessa? Você está enxergando coisas.

`Math.floor(num)` Arredonda um número com vírgula para baixo para o próximo inteiro.\
`Math.floor(4.2)` 4\
`Math.floor(-4.2)` -5 🙄

`Math.pow(base, exp)` Retorna a base elevada ao expoente. Igual a qual operador que você já viu 🧠 ? Isso mesmo `**`.\
`Math.pow(4,2)` 16. Repare na vírgula, pois são dois argumentos.

`Math.round(num)` Arredonda um número com vírgula para o número inteiro mais próximo. A partir de .5 na parte fracionária ele arredonda para cima. Antes de .5, é arredondado para baixo.\
`Math.round(2.7)` 3\
`Math.round(2.4)` 2

`Math.trunc(num)` Retira a parte fracionária de um número.\
`Math.trunc(4.2)`4

`Math.sqrt(num)` Esse você já viu nesta aula. Ele retorna a raiz quadrada de um número.\
`Math.sqrt(36)` 6

`Math.min()` e `Math.max()` Retornam o valor menor e o valor maior de uma sequência de números, respectivamente.\
`Math.min(0, 150, 43, 20, -8, -200)` -200\
`Math.max(0, 150, 42, 20, -8, -200)` 150

`Math.random()` Esse é bem legal. Ele gera um número aleatório entre 0 e 1 (1 não incluso). Já vejo você pensando em aplicar em seus jogos. E obviamente a cada vez que você fizer o número vai ser diferente. Mas tem uns macetes para você usar essa função:
Você já sabe que o número retornado é um número fracionário que fica entre 0 e 1, e isso não parece muito útil. Mas se você multiplicar esse número por outro e aplicar a função `Math.floor()`, você terá um inteiro aleatório até esse número.
Para gerar um número aleatório de 0 até 9:\
`Math.floor(Math.random() * 10)`\
Para gerar um número aleatório de 0 até 10:\
`Math.floor(Math.random() * 11)`\
Para gerar um número aleatório de 0 até 100:\
`Math.floor(Math.random() * 101)`\
Para gerar um número aleatório de 1 até 100:\
`Math.floor(Math.random() * 100) + 1`Repare que estamos somando 1 no final então o resultado anterior que daria até 99, agora chega a 100. Acho que você pegou a ideia.

Ainda temos as funções `Math.log()` , `Math.log2()` , `Math.log10()` , `Math.sign()` , `Math.sin()` , `Math.cos()` , `Math.tan()`. E também as **constantes** `Math.PI` , `Math.E` , `Math.SQRT2` e outras para você brincar 👩‍🚀 e usar nos seus jog...programas.',
3)

# Classe Condicionais    class_id 4

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Condicionais If Else', 'SYS', 1, 'If, Else, Else if', 'Agora seu treinamento inicial está completo, arquétipo de herói 🧙 . Você já está pronto para programar jog...softwares e apps mais complexos. Pegue seu sabre de luz, treine arduamente (revise o material das últimas aulas. Estamos tentando dar um ar de fantasia, mas estamos falhando miseravelmente 😔 ) e se prepare para o desafio que o encontra no mundo perigoso...OK 😞 . Vamos parar com isso. O conteúdo desta aula trata de **condicionais**, uma maneira de você realizar instruções diferentes com base em condições diferentes. Isso permite a você alterar o fluxo de execução do seu programa, e é uma das bases dos algoritmos (as outras são **repetições** e **funções** ⏳ ).
Para estruturar um condicional a primeira palavra chave que você precisa saber é `if` ou "se" em inglês. A sintaxe (forma de escrever de uma maneira que o computador entenda) correta do `if` é feita assim:\
`if (condição) instrução a ser feita`\
Ou com um exemplo prático:\
`let idade = Number(prompt("Entre com a sua idade"))`\
`if (idade >= 18) alert("Você é maior de idade.")`
E a mensagem `Você é maior de idade` aparecerá no janela do seu browser se você tiver 19 ou mais anos (não vale enganar hem).
Se o número for menor que 18 nada acontece. Por quê 🤷 ? Pela razão de você ter usado um condicional `if`, e a instrução dentro do bloco `if` (vamos explicar isso logo adiante) só ocorre se a condição é verdadeira, ou seja, quando a idade é maior ou igual a 18.

"E se eu quiser realizar mais do que uma instrução dentro do if ?". Isso é possível, e vamos apresentar mais um símbolo na sintaxe de JS (e de várias linguagens), o `{ }`, chaves em português, ou braces em inglês 💂 . Garantimos que você vai usar muito esse símbolo nos seu programas. Sem adiantar muito o contéudo, as `{ }` chaves delimitam um **escopo** em que só o que você colocou entre as chaves é executado, dentro desse escopo. Ficou complicado? Com alguns exemplos a gente descomplica:

<code>
let ano = 2001
if (ano == 2001) {
    alert("Nesse ano começou o século XXI")
    alert("O filme " + ano + " é uma ótima ficção científica")
    alert("Nesse ano ocorreram os atentados de 7 de Setembro")
}
alert("Esse texto vai ser exibido independente da condição ser satisfeita porque está fora do bloco if")
</code>

Ficou um pouco mais claro 🔆 agora ? "E se a condição não for satisfeita"? Bom, no caso, o que está dentro dos parênteses retorna `false`, e todo o bloco dentro do `if` não será executado.

<code>
let anoPrompt = prompt("Entre com um ano. Pode ser o ano atual ou o do seu nascimento.")
if (anoPrompt == 2001) {
    alert("Nesse ano começou o século XXI")
    alert("O filme " + ano + " é uma ótima ficção científica")
    alert("Nesse ano ocorreram os atentados de 7 de Setembro")
}
alert("A condição do if não foi satisfeita, então tudo o que está entre chaves não será exibido")
</code>

OK. É bastante informação. Pense um pouco no que você acabou de ver 🤔 .Agora, uma dúvida deve ter surgido no último exemplo. o JS tem alguma maneira de executar o código quando a condição não é alcançada? A resposta é sim e você usará a palavra chave `else`, ou senão em português. Ela executa tudo o que não foi executado **imediatamente** depois do `if` (o que é `false` para o condicional). Então:

<code>
let marioEstaGrande = false
let vidasMario = 3
/*marioEstaGrande quer dizer que nosso Mário pegou um cogumelo vermelho e está em formato maior (pode ser atingido duas vezes). Mas nesse exemplo o nosso Mário não pegou nenhum cogumelo e está pequeno.
Digamos que se passa um tempo e em alguma situação do jogo o mário é atingido por um inimigo: em um jogo existe um loop (game loop) que fica constantemente rodando para verificar essas situações (eventos) */
if (marioEstaGrande){
    marioEstaGrande = false //agora ele está pequeno porque foi atingido
} else (merioEstaGrande) { //ele está pequeno porque não pegou nenhum cogumelo vermelho e é nessa situação que esse exemplo irá cair
    vidasMario-- //agora será 2
}
//e o ciclo retorna para a próxima vida do Mário
</code>

Mais um exemplo? Vamos lá:

<code>
let meuPokemonFavorito = "Charmander"
let seuPokemonFavorito = prompt("Qual o seu Pokemon favorito?")
if (meuPokemonFavorito === seuPokemonFavorito) {
    alert("Nossos Pokemons favoritos são iguais!!!")
} else {
    alert("Nossos Pokemons favoritos são diferentes.")
}
</code>

Você notou que utilizamos o operador `===` 🧠 ? É bom você se acostumar a utilizar esse operador ao invés do `==` para evitar futuras dores de cabeça 🤕 . Temos tamém a palavra-chave `else if`, senão se. E ela é assim mesmo, com um espaço no meio (um dos raros casos na sintaxe de JS). Você a usará para encadear várias sequências de condicionais que não foram satisfeitas no `if` inicial. Assim como o `if`, ela requer uma condição em parênteses.
Um exemplo:

<code>
let anoEntrada = prompt("Em que ano o Javascript foi criado?");
if (anoEntrada < 1995) {
    alert("Muito cedo...");
} else if (anoEntrada > 1995) {
    alert("Muito tarde");
} else {
    alert("Acertou. Cacildis!");
}
</code>

E você pode colocar vários `if else` após um `if`. Isso vai depender da complexidade da **lógica** do seu programa. Uma outra coisa importante que você deve ter notado: dentro de um bloco `{ }` o texto está recuado um pouco. Isso se chama **IDENTAÇÃO** e é uma ótima prática de programação. Hoje em dia a maioria dos editores de código faz isso automaticamente, mas no tempo antigo 🧙 a identação era feita com a tecla TAB, ou usando espaços (geralmente 4) — e guerras ferrenhas eram travadas entre os programadores 🧑‍💻 ⚔ para decidir qual método era o melhor (evidentemente não se chegou a nenhuma conclusão e os programadores voltaram a jogar multiplayer para se chegar a um armistício e terminarem as guerras, que se tornaram virtuais).',
4);

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Operadores Lógicos', 'SYS', 2, 'Operadores && , || , !', 'Para essa aula recomendamos que você esteja com o raciocínio afiado. Jogue uma palavra-cruzada, um sudoku, até um Candy Crush vale 😉 . Dentro do `if` ou do `if else` da nossa aula anterior nós verificamos algumas condições, se um número ou um texto é igual a outro número ou texto, se um número é maior ou menor do que outro.

Até aí OK, nada demais. Mas suponhamos que além no nosso condicional verificar uma só situação não basta. Nós queremos testar além dos comparadores básicos, ou duas ou mais condições **ao mesmo tempo** no mesmo `if`. Aqui entram os operadores lógicos `||` "Or", `&&` "And" e `!` "Not" ("ou", "e", "não" respectivamente). Como eles funcionam 🙇 ? Calma, eles são bem intuitivos.

O de negação simplesmente nega um valor Boolean. O que é `true` vira `false`, e o que era `false` vira `true`. Ou V vira F, e F vira V. Ou se p é uma proposição do tipo : "Sócrates é um jogador de futebol". A negação é: "Sócrates não é um jogador de futebol" ou "Não é verdade que Sócrates é um jogador de futebol".
| p | !p |
|---|----|
| V | F |
| F | V |
"E para eu aplicar nos meu jog..programas!!! Que utilidade tem isso ?": 🤬 . Vamos mostrar com exemplos. p vai ser "o número é 42", e q vai ser "o planeta é a Terra".

<code>
let num = 42
let planeta = "Terra"
//no primeiro caso da tabela de negação temos p, "o número é igual a 42"
//então
let numIgual42 = (num === 42) //vai começar como true
//se aplicarmos o operador
numIgual42 = !numIgual42 //agora ele é false, mas perceba que num continua tendo o valor 42
</code>

Com os outros operadores a importãncia do Not vai ficar mais clara. Prometemos 🤞 . O operador `&&`, And só retorna verdadeiro se todas as comparações forem verdadeiras; no nosso caso quando ambos p **e** q forem verdadeiros. Quando "O número é 42" **E** "O planete é a Terra". Sinto que você quer uma tabela. Você é um/uma maniáco/a por tabelas e gráficos do Excel 📊 . Posso ver nos seus olhos. Essa é a tabela do `&&`:
| p | q | p && q |
|---|---|--------|
| V | V | V |
| V | F | F |
| F | V | F |
| F | F | F |

Agora já temos um operador a mais para brincar. Então:

<code>
if(num === 42 && planeta === "Terra") { ...}
if(num < 42 && planeta === "Marte") {...}
if(planeta !== "Terra" && num === 42) {..}
if(planeta !== "Vênus" && planeta !== "Júpiter") {...}
if(num <= 40 && num >= 42) {...}
</code>

Olhe a tabela verdade acima e tente descobrir em que blocos de `if` nosso programa entraria. É um bom exercício de lógica. E por último temos o operador `||`, Or e ele retorna verdadeiro quando **ao menos uma** condição é verdadeira; no caso de p **ou** q forem verdadeiros, ou um ou outro, tanto faz. Quando "O número é 42" **OU** "O planeta é a Terra". Nem vou perguntar, você já sabe o que é, seu/sua maniáco/a por números.
| p | q | p || q |
|---|---|--------|
| V | V | V |
| V | F | V |
| F | V | V |
| F | F | F |
',
4);

INSERT INTO `tutorials`(`title`,`sys_id`, `position`, `description`, `content`, `class_id`) 
VALUES ('Operador Ternário', 'SYS', 3, 'Operador () ? :', 'Essa aula vai ser tão fácil que você vai conseguir completá-la de olhos fechados 😎 💤. Ok. Não tão fácil assim. Um operador ternário nada mais é que um `if - else` mais rebuscado que as vezes torna seu programa mais legível, e nós gostamos disso, não? A sintaxe do operador é dessa maneira: `let resultado = condição ? valorRetornadoSeTrue : valorRetornadoSeFalse`. Você vai utilizá-lo principalmente para atribuir valor a suas variáveis ou constantes. Vamos dar um exemplo:
<code>
let idade = prompt("Qual a sua idade? E não minta. Temos meios de averiguar")
let acessoPermitido = (idade >= 18) ? "Pode entrar" : "Acesso Barrado!!!"
alert(acessoPermitido)
</code>

Barbadinha né? Você pode encadear vários `? :` depois do primeiro condicional, exatamente como se fossem `if else` 🧠 . Um exemplo? Pode deixar:
<code>
let anos = prompt("Sua idade sem palhaçadas? Os métodos anteriores de averiguação falharam", 18)
let mensagem = (anos < 3) ? "Cuti cuti!" :
  (anos < 18) ? "Olá mini adulto!"" :
  (anos < 100) ? "Oi adulto!" :
  "Caramba! É você Gandalf ?"

alert(mensagem)
</code>',
4);
