Você viu na aula anterior que nosso querido JS, apesar de ser uma ótima linguagem de programação é um pouco sacana 🤡 no que se refere a **tipos**. Ele tem uma tendência grande a converter tipos diferentes, e os resultados podem deixar você perplexo. Mas não se você estudar um pouco como o JS lida com os tipos quando usamos seus **operadores**.
Para aquecer 🧣🧤 vamos falar sobre um operador que nós já vimos, o `+`, mas em outro contexto, com **Strings**. Você pode concatenar (juntar) duas ou mais strings utilizando esse operador. Por exemplo:\
`let a = "Goku"`\
`let b = "Naruto"`\
`let c = "Luffy"`\
`alert(a + ", " + b + " e " + c + " são meus personagens de anime favoritos")`Se você não gostou do exemplo, coloque os seus personagens 😝 .\
Bem legal, não? Repare que cada `+` precisa ter duas Strings ao seu lado, e que a vírgula e o "e" foram colocados em aspas duplas (poderiam ter sido aspas simples). Agora vem a parte estranha. Se você fizer:\
`alert("1" + 2)`\
12 aparecerá no sua janela do browser. Isso ocorre porque JS converte o tipo Number para String quando fazemos uma concatenação entre os dois. O operador `+` deixa de ser um operador de soma e vira um operador de concatenação quando encontra um String.
Mas ele tem ordem de precedência, então:\
`alert(2 + 2 + "1")`\
é igual a "41" e não "221", porque a soma entre Numbers está localizada antes do operador de concatenação de Strings. Agora:\
`alert("1" + 2 + 2)`\
é igual a "122" e não "14", porque o JS transforma tudo em String após a primeira concatenação (lembre-se que o número 2 é convertido para String automaticamente).

Você pode forçar alguma variável ou valor a se transformar 🦸 em um tipo. Vamos ver como transformar para Number, String e Boolean.

# Number

Você pode forçar um valor a ser um número de várias maneiras. Por exemplo:\
`alert("6"/"2")` Converte as Strings para Numbers e faz a divisão e imprime 3\
`let a = false`\
`a = +a`\
`alert(a, typeof a)`\
Aqui utilizamos o operador unário `+` para converter um Boolean para um Number. Temos mais uma maneira, utilizando a função `Number()`. Assim:\
`let str = "123"`\
`let num = Number(str)`\
`` alert(`número de ${num} de tipo ${typeof num}`)``
E se a String conter algo que não for um número?\
`let titulo = Number("Just Dance 2021")`\
`alert(titulo)`Você se lembra do que significa NaN ?\
Outras conversões são possíveis:\
`alert(Number(true))` 1\
`alert(Number(false))` 0\
`alert(Number(null))` 0\
`alert(Number(undefined))` NaN\
`alert(Number(""))` String vazia é igual a 0\
`alert(Number(" 42 "))` 42

# String

Se com números as coisas são um pouco complicadas, com Strings são bem fáceis. Tudo é convertido para um texto, seja `true`, `false`, `null`, `undefined`, `42`, `3.1416`. Tente:\
`let valor = String(null)`\
`` alert(`conteúdo: ${valor}, do tipo: ${typeof valor}`) ``

# Boolean

Aqui se aplica uma regrinha:
Todos os valores que por intuição consideramos "vazios" se tornam **falsos** ou `false`. Isso inclui:

- 0 (O número zero)
- "" (uma String vazia)
- null
- undefined
- NaN

Todo o resto vira verdadeiro ou `true`. Então:\
`Boolean(1)` true\
`Boolean(0)` false\
`Boolean("")` false\
`Boolean("texto")` true\
`Boolean("0")` true\
`Boolean(" ")` true, por causa do espaço no meio\
`Boolean(null)`false\
`Boolean(undefined)`false\
`Boolean(NaN)`false

Ufa ! 😅 acabamos essa aula. Eu já vejo os seus poderes aumentarem. Siga treinando 🏋 e na próxima aula eu irei ensinar como aplicar a técnica do Kamehameha 🌠. Não? Serão conceitos de programação mesmo 😶 .
