Nós acabamos de ver operadores que fazem operações matemáticas. Mas nós não conseguimos programar softwares complexos ou jogos só com operadores matemáticos. Imagine que seu program ou jogo tenha que checar se alguma variável atingiu determinado valor, ou uma variável Boolean está verdadeira. Aí que entram os **operadores de comparação**. Temos basicamente esses operadores, que também devem lembrar o que você deve ter visto nas suas adoradas aulas de matemática 😑 :

- Igual a `a == b`, e repare nos dois caracteres `=`, para não se confundir com o operador de atribuicão. Isso é um erro **MUITO COMUM** para quem está iniciando, e até mesmo para quem se diz avançado 👽 . Quando os dois **operandos** (que são as variáveis dos dois lados do operador, no caso `a` e `b`) são iguais, esse operador retorna verdadeiro, ou em JS `true`;
- Maior/Menor `a > b, a < b`. O primeiro verifica se a é maior do que b, e o segundo se a é menor do que b. Bem simples não? No primeiro caso se `a` for menor do que `b`, o operador retorna `true`. No segundo caso... Você pegou a ideia né? Você é um/a aluno/a inteligente.
- Maior ou igual/Menor ou igual `a >= b, a <= b`. Semelhante ao anterior, mas e isso é **IMPORTANTE**, também é verdadeiro no caso dos operandos serem iguais. Exemplos:\
  `5 >= 7` false\
  `0 <= 1` true\
  `-1 <= 0` true\
  `42 <= 42` true, porque os dois são iguais\
  `42 < 42` false, obviamente\
  Acho que você pegou o jeito. A diferença entre esses dois operadores vai ficar mais clara quando entramos em Repetições e Laços ⏳.
- Diferente de `a != b`. Esse operador verifica se os dois operandos são diferentes, e se for o caso, retorna `true`. Ele faz exatamente o contrário do operador igual `==`.

E esses operadores podem ser usados em textos ou Strings em JS? A resposta é sim, mas os resultados são um tanto estranhos 😵 (exceto o comparador de igualdade `==` e o de diferente de `!=` ). Basicamente JS compara as duas Strings por ordem alfabética, letra por letra, mas há alguns detalhes no algoritmo que levam a conclusões não tão óbvias. De qualquer maneira você pode tentar no seu terminal algumas comparações com Strings, mas não vamos abordar esse assunto aqui.

Há um último detalhe, e isso se aplica especificamente a JS. Como a linguagem é fracamente tipada, alguns bugs aconteciam, ou por descuido do programador, ou por detalhes na implementação do JS que convertem o tipo por debaixo dos panos 😼 .
Se você fizer:\
`alert(0 == false)`\
`alert('' == false)`\
`alert(1 == true)`\
Você terá `true` em todos os casos 🤯 . Isto ocorre porque JS converte tipos com o uso de alguns operadores. Você verá mais sobre conversão de tipos em uma próxima aula. Para garantir que você quer saber que as variáveis tem os valores iguais e seus tipos também são iguais temos o operador `===` ou **Strict Equality** (fale com um sotaque britânico 💂 para ficar mais pomposo). Ele assegura que além do valor, o tipo tem que ser igual. então:\
`alert(0 === false)`\
false, porquê são de tipos diferentes (Number e Boolean)\
`alert('' === false)`\
false também, (String e Boolean)\
`alert(1 === true)`\
false também, (Number e Boolean)\
Para finalizar, temos o **Strict non-equality** `!==` que é análogo ao operador diferente de `!=`, mas assegurando que além dos valores, os tipos também são diferentes.
